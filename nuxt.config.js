const pkg = require('./package');

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=latin-ext' },
    ],
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/datepicker',
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    'nuxt-sass-resources-loader',
    '@nuxtjs/axios',
  ],

  /*
  ** SASS globals
  */
  sassResources: [
    '@/assets/style/_variables.scss',
    '@/assets/style/_svg-icons.scss',
    '@/assets/style/lib/shevy/_variables.scss',
    '@/assets/style/lib/shevy/_functions.scss',
    '@/assets/style/lib/shevy/_mixins.scss',
  ],

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }
      // Override url-loader test (exclude svg to be able to use vue-svg-loader)
      config.module.rules
        .filter(rule => rule.test && /svg/.test(rule.test.toString()))
        .forEach((rule) => {
          rule.test = /\.(png|jpe?g|gif)$/; // eslint-disable-line no-param-reassign
        });

      config.module.rules.push({
        test: /\.svg$/,
        loader: 'vue-svg-loader',
      });
    },
  },
};
