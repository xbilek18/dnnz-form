import AirbnbStyleDatepicker from 'vue-airbnb-style-datepicker';
import Vue from 'vue';

Vue.use(AirbnbStyleDatepicker);
