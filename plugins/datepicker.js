import Vue from 'vue';
import AirbnbStyleDatepicker from 'vue-airbnb-style-datepicker';
import 'vue-airbnb-style-datepicker/dist/vue-airbnb-style-datepicker.min.css';

const datepickerOptions = {
  sundayFirst: false,
  dateLabelFormat: 'dddd, MMMM D, YYYY',
  days: ['Pondělí', 'Úterý', 'Středa', 'Čtvrtek', 'Pátek', 'Sobota', 'Neděle'],
  daysShort: ['Po', 'Út', 'St', 'Čt', 'Pá', 'So', 'Ne'],
  monthNames: [
    'Leden',
    'Únor',
    'Březen',
    'Duben',
    'Květen',
    'Červen',
    'Červenec',
    'Srpen',
    'Září',
    'Říjen',
    'Listopad',
    'Prosinec',
  ],
  colors: {
    selected: '#E42026',
    inRange: '#f56065',
    selectedText: '#fff',
    text: '#191919',
    inRangeBorder: '#E42026',
    disabled: '#fff',
  },
  texts: {
    apply: 'Potvrdit',
    cancel: 'Zpět',
  },
};

Vue.use(AirbnbStyleDatepicker, datepickerOptions);
